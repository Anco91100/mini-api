const express = require('express');
const router = express.Router();
const orderController = require('../controllers/order');
const {verifyToken,isEnterprise, isUser} = require('../middleware');

router.put('/update',[verifyToken, isEnterprise],orderController.update);
router.get('/orders', [verifyToken, isUser], orderController.orders);


module.exports = router;
const express = require('express');
const router = express.Router();
const paymentController = require('../controllers/payment');
const {verifyToken,isUser} = require('../middleware');

router.post('/buy',[verifyToken, isUser],paymentController.buy);

module.exports = router;
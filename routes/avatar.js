const express = require('express');
const router = express.Router();
const avatarController = require('../controllers/avatar');
const {verifyToken,isUser} = require('../middleware');

router.post('/create',[verifyToken, isUser],avatarController.create);

module.exports = router;
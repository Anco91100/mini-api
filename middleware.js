require('dotenv').config();
const jwt = require('jsonwebtoken');

function generateAccessToken({user, role}) {
    return jwt.sign({user: user, role: role}, process.env.ACCESS_TOKEN_SECRET, {expiresIn: "15m"}) 
}

// refreshTokens
let refreshTokens = []

function generateRefreshToken({user, role}) {
    const refreshToken = jwt.sign({user: user, role: role}, process.env.REFRESH_TOKEN_SECRET, {expiresIn: "20m"})
    refreshTokens.push(refreshToken)
    return refreshToken
}

function verifyToken(req, res, next) {
    const authHeader = req.headers["cookie"];
    const token = authHeader.split("=")[1];

    if (token == null) res.sendStatus(400).send("Token not present");

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err,user) => {
        if(err){
            res.status(403).send("Token invalid");
        }
        else {
            req.user = user;
            next();
        }
    })

}


function isEnterprise(req, res, next) {
    console.log(req.user)
    if(req.user && req.user.role === 'Enterprise')
        next();
}

function isUser(req, res, next) {
    console.log(req.user)
    if(req.user && req.user.role === 'User')
        next();
}


module.exports = {
    generateAccessToken,
    generateRefreshToken,
    refreshTokens,
    verifyToken,
    isUser,
    isEnterprise
}
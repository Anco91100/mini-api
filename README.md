Node 16
npm 8.1.2

Configurez le .env (s'inspirer du .env.example)

Routes :
    Client :
        POST /client/create body {username,email,password}
        POST /client/login  body {email,password}
        POST /client/logout
        DELETE /client/delete body {email}
    Avatar :
        POST avatar/create
    Token :
        POST token/refreshToken body {token: refreshToken}
    Payment :
        POST payment/buy body {quantity}
    Order :
        PUT /order/update body {orderNumber : id (Orders.id),statusOrder:message}
        GET /order/orders

Avec le docker-compose :

Placez vous dans le container mysqldb et 
faites mysql -u root -p votre-database-name < docker-entrypoint-initdb.d/script.sql
Crée une base de donnée avant.

Pour connaitre l'ip de l'app faites docker inspect app | grep IPAddress
Installez Postman pour la partie 2 pour gérer les clients

Si la database n'est pas crée, faites docker-compose exec mysqldb bash puis mysql -u root -p tapez password et ensuite CREATE DATABASE votre-db-name;
Ensuite docker-compose up --build --force-recreate --no-deps -d app

Si vous avez cette erreur "Client does not support authentication protocol requested by server; consider upgrading MySQL client"
connectez vous à la db - >docker-compose exec mysqldb bash puis mysql -u root -p tapez password
et ensuite faites ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
et ensuite tapez flush privileges;
Ensuite docker-compose up --build --force-recreate --no-deps -d app

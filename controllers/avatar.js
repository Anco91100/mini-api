const qs = require('../lib/avatar_query')

async function create(req,res) {
    qs.createAvatar(req,res);
}

module.exports = {
    create
}
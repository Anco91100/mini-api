const qs = require('../lib/user_query');

async function create(req, res){
    qs.createUser(req,res);
}

async function login(req, res){
    qs.loginUser(req, res)
}

function logout(req, res) {
    qs.logout(req,res)
}

function deleteUser(req,res) {
    qs.deleteUser(req,res)
}

module.exports = {
    create,
    login,
    logout,
    deleteUser
}
const token = require('../middleware');
const maxAge = 3 * 24 * 60 * 60 * 1000;

function refreshToken(req,res) {
    if (!token.refreshTokens.includes(req.body.token)) 
    res.status(400).send("Refresh Token Invalid")
    
refreshTokens = token.refreshTokens.filter( (c) => c != req.body.token)

const accessToken = token.generateAccessToken({user: req.body.username})
const refreshToken = token.generateRefreshToken({user: req.body.username})
res.cookie('jwt', accessToken, {httpOnly: true, maxAge});   

res.json ({accessToken: accessToken, refreshToken: refreshToken})
}

module.exports = {
    refreshToken
}
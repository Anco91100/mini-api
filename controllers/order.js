const qs = require('../lib/order_query');

async function update(req, res) {
    qs.updateOrder(req,res);
}

async function orders(req,res) {
    qs.getOrders(req,res);
}

module.exports = {
    update,
    orders
}
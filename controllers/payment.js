const qs = require('../lib/payment_query');

function buy(req, res) {
    qs.buy(req,res);
}

module.exports = {
    buy
}
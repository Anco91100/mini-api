/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Avatar`
--

DROP TABLE IF EXISTS `Avatar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Avatar` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `id_user` int NOT NULL,
  `name` varchar(1000) NOT NULL,
  `options` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `Avatar_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `User` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Avatar`
--

LOCK TABLES `Avatar` WRITE;
/*!40000 ALTER TABLE `Avatar` DISABLE KEYS */;
INSERT INTO `Avatar` VALUES (10,4,'grady@yahoo.fr','lunette orange');
/*!40000 ALTER TABLE `Avatar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Factory`
--

DROP TABLE IF EXISTS `Factory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Factory` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_user` int NOT NULL,
  `id_order` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_order` (`id_order`),
  CONSTRAINT `Factory_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `User` (`id`),
  CONSTRAINT `Factory_ibfk_2` FOREIGN KEY (`id_order`) REFERENCES `Orders` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Factory`
--

LOCK TABLES `Factory` WRITE;
/*!40000 ALTER TABLE `Factory` DISABLE KEYS */;
INSERT INTO `Factory` VALUES (1,4,5),(2,4,6),(3,4,7);
/*!40000 ALTER TABLE `Factory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Mini`
--

DROP TABLE IF EXISTS `Mini`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Mini` (
  `id` int NOT NULL AUTO_INCREMENT,
  `PRICE` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Mini`
--

LOCK TABLES `Mini` WRITE;
/*!40000 ALTER TABLE `Mini` DISABLE KEYS */;
INSERT INTO `Mini` VALUES (1,15);
/*!40000 ALTER TABLE `Mini` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Offer`
--

DROP TABLE IF EXISTS `Offer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Offer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_package` int NOT NULL,
  `pack` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_package` (`id_package`),
  CONSTRAINT `Offer_ibfk_1` FOREIGN KEY (`id_package`) REFERENCES `Package` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Offer`
--

LOCK TABLES `Offer` WRITE;
/*!40000 ALTER TABLE `Offer` DISABLE KEYS */;
/*!40000 ALTER TABLE `Offer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Orders`
--

DROP TABLE IF EXISTS `Orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Orders` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `id_user` int NOT NULL,
  `product_id` int NOT NULL,
  `quantity_order` int NOT NULL,
  `package_id` int DEFAULT NULL,
  `gift_user` int DEFAULT NULL,
  `name_user` varchar(1000) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `order_price` float NOT NULL,
  `status_order` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `gift_user` (`gift_user`),
  KEY `product_id` (`product_id`),
  KEY `package_id` (`package_id`),
  CONSTRAINT `Orders_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `User` (`id`),
  CONSTRAINT `Orders_ibfk_2` FOREIGN KEY (`gift_user`) REFERENCES `User` (`id`),
  CONSTRAINT `Orders_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `Mini` (`id`),
  CONSTRAINT `Orders_ibfk_4` FOREIGN KEY (`package_id`) REFERENCES `Package` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Orders`
--

LOCK TABLES `Orders` WRITE;
/*!40000 ALTER TABLE `Orders` DISABLE KEYS */;
INSERT INTO `Orders` VALUES (4,4,1,55,2,NULL,'grady@yahoo.fr','MiNi','MiNi Crowd',495,'Pending'),(5,4,1,55,2,NULL,'grady@yahoo.fr','MiNi','MiNi Crowd',495,'Delivered'),(6,4,1,55,2,NULL,'grady@yahoo.fr','MiNi','MiNi Crowd',495,'Dispateched'),(7,4,1,55,2,NULL,'grady@yahoo.fr','MiNi','MiNi Crowd',495,'Pending');
/*!40000 ALTER TABLE `Orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Package`
--

DROP TABLE IF EXISTS `Package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Package` (
  `id` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(1000) NOT NULL,
  `discount` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Package`
--

LOCK TABLES `Package` WRITE;
/*!40000 ALTER TABLE `Package` DISABLE KEYS */;
INSERT INTO `Package` VALUES (1,'MiNi Family',0.2),(2,'MiNi Crowd',0.6);
/*!40000 ALTER TABLE `Package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `User` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `password` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (4,'$2b$10$F7SpI0WxCAH4XFT6Mn4rcuXSzSO6sK/xwXPQk59VQCxkqyOjZmbBe','grady','grady@yahoo.fr','User'),(5,'$2b$10$ocJdnTY7oye8z2Lb.1k4je4yzpZJJwhG294jTo5J9SK2Xe1xnosme','mini','grady@mini.fr','Enterprise'),(6,'$2b$10$6x3obUgqiVXtqtgplPFhxuCZq/PlKDcSxq85.SJ5A8T9rJfT7gxqu','oui','grady@esl.fr','User'),(7,'$2b$10$t4r19afeVAZ2/IsypCZEpOjj4DwISt3rLca2dsblkTTB2TbsjZlBe','sncf','grady@ouisncf.fr','Enterprise');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-03 22:42:11

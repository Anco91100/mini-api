var mysql = require('mysql');
require('dotenv').config();

var con = mysql.createPool({
  host: process.env.DB_HOST || process.env.HOST_MYSQL,
  user: process.env.MYSQLDB_USER || process.env.USER_MYSQL,
  password: process.env.MYSQLDB_ROOT_PASSWORD || process.env.PASSWORD_MYSQL,
  database: process.env.MYSQLDB_DATABASE || process.env.DATABASE_MYSQL,
  port:process.env.MYSQLDB_DOCKER_PORT || process.env.DB_PORT
});

con.getConnection(function(err) {
  if (err) throw err;
});

module.exports = {
    con
}


require('dotenv').config();

const express = require('express');
const app = express();
const port = process.env.NODE_DOCKER_PORT || process.env.PORT;
const cors = require('cors');
const bodyParser = require('body-parser');

const userRoute = require('./routes/user');
const avatarRoute = require('./routes/avatar');
const paymentRoute = require('./routes/payment');
const orderRoute = require('./routes/order');
const tokenRouter = require('./routes/token');
const {verifyToken} = require('./middleware');

app.listen(port, () => {
    console.log(`Server Started on port ${port}`)
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use('/user',userRoute);
app.use('/avatar', avatarRoute);
app.use('/payment/', paymentRoute);
app.use('/order', orderRoute);
app.use('/token',tokenRouter);

app.get('*', verifyToken, (req, res) => {
    res.send(`${req.user.user} successfully connected`);
})

module.exports = app;
const db = require('../db').con;
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const token = require('../middleware');
const maxAge = 3 * 24 * 60 * 60 * 1000;

async function createUser(req, res) {
    const username = req.body.username;
    const email = req.body.email;
    const password = req.body.password;
    let siret = 0;
    if(req.body.siret){
        siret = req.body.siret;
    }
    const hashPassword = await bcrypt.hash(password,10);
    db.getConnection(async(err, connection) => {
        if(err) throw(err);

    const sqlSearch = "SELECT * FROM User Where email = ?";
    const search_query = mysql.format(sqlSearch,[email]);

    const sqlInsert = "INSERT INTO User VALUES(0,?,?,?,?)";
    let insert_query;
    if(siret !== 0){
        insert_query = mysql.format(sqlInsert,[hashPassword, username, email,'Enterprise']);
    }
    else {
        insert_query = mysql.format(sqlInsert,[hashPassword, username, email,'User']);

    }

    await connection.query( search_query, async(err, result) => {
        if(err) throw(err);
        
        if(result.length !== 0){
            connection.release();
            console.log('User already exists');
            res.sendStatus(409);
        }
        else {
            await connection.query(insert_query, async(err, result) => {
                connection.release();
                if(err) throw(err);
                console.log('User createad', result.insertId);
                res.sendStatus(201);
            })
        }
    })
    })
}

function loginUser(req, res){
    const email = req.body.email;
    const password = req.body.password;

    db.getConnection( async(err, connection) => {
        if(err) throw(err);

        const sqlSearch = 'SELECT * FROM User WHERE email = ?'
        const search_query = mysql.format(sqlSearch,[email]);

        await connection.query(search_query, async(err, result) => {
            if(err) throw(err);

            if(result.length === 0){
                console.log('User does not exist');
                res.sendStatus(404);
            }
            else {
                const hashPassword = result[0].password;
                if(await bcrypt.compare(password, hashPassword)){
                    const accessToken = token.generateAccessToken({user: email, role: result[0].role});
                    const refreshToken = token.generateRefreshToken({user: email, role: result[0].role});
                    res.cookie('jwt', accessToken, {httpOnly: true, maxAge});   
                    res.status(200).json({email: email,accessToken: accessToken, refreshToken: refreshToken});
                }
                else {
                    console.log('Password incorrect')
                    res.send(`Password incorrect`)
                }
            }
        })
        
    })
}

function logout(req,res) {
    res.cookie('jwt', '', {maxAge:1});
    refreshTokens = token.refreshTokens.filter( (c) => c != req.body.token)
    //remove the old refreshToken from the refreshTokens list
    res.status(204).send("Logged out!")
}

function deleteUser(req,res) {
    const email = req.body.email;
    const deleteUser = 'DELETE FROM User WHERE email = ?';
    const deleteUser_query = mysql.format(deleteUser,[email])
    db.getConnection(async(err, connection) =>{
        await connection.query(deleteUser_query, (err,result) => {
            if(err) throw(err)
                res.status(200).send('Deleted');
        })
    })
}

module.exports = {
    createUser,
    loginUser,
    logout,
    deleteUser
}
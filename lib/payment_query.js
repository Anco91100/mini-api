const db = require('../db').con;
const mysql = require('mysql');

function buy(req,res){
    const quantity = req.body.quantity;
    db.getConnection( async(err, connection) => {
        if(err) throw(err);

        const sqlSearchUserID = 'SELECT id, email FROM User WHERE email = ?'
        const searchUserID_query = mysql.format(sqlSearchUserID,[req.user.user])

        await connection.query(searchUserID_query, async (err, user) => {
            if(user.length !== 0){
                const sqlProductID = 'SELECT id, Price FROM Mini';
                const product_query = mysql.format(sqlProductID)
                await connection.query(product_query, async (err2,product) => {
                    if(product.length !==0){
                        if(quantity >= 50){
                            const sqlPackageSearch = 'SELECT * FROM Package WHERE Name = ?';
                            const package_query = mysql.format(sqlPackageSearch, ['MiNi Crowd']);
                            await connection.query(package_query, async (err3,packageRes) => {
                                const discount_price = product[0].Price * quantity * packageRes[0].discount;
                                const sqlOrder = 'INSERT INTO Orders(id_user,product_id,quantity_order,package_id,name_user,product_name,package_name,order_price,status_order) VALUES(?,?,?,?,?,?,?,?,?)';
                                const order_query = mysql.format(sqlOrder,[user[0].id,product[0].id,quantity,packageRes[0].id,req.user.user,'MiNi',packageRes[0].Name,discount_price,'Pending']);
                                await connection.query(order_query, async (err4,order) => {
                                    if(order.length !== 0){
                                        const insertInFactoy = 'INSERT INTO Factory VALUES(0,?,?)'
                                        const insertInFactoy_query = mysql.format(insertInFactoy,[user[0].id,order.insertId]);
                                        await connection.query(insertInFactoy_query, (err5, result) => {
                                            connection.release();
                                            res.status(200).json({message:'Ordered !'})
                                        })
                                    }
                                })
                            })
                        }
                    }
                })
            }
        })
    })
}

module.exports = {
    buy
}
const mysql = require('mysql');
const db = require('../db').con;

function updateOrder(req,res) {
    const orderNumber = req.body.orderNumber;
    const statusOrder = req.body.statusOrder;
    const sqlSearchUserID = 'SELECT id FROM User WHERE id = ?'
    const searchUserID_query = mysql.format(sqlSearchUserID,[orderNumber])

    db.getConnection(async (err,connection)=> {
        await connection.query(searchUserID_query, async (err, result) => {
            if(result.length !== 0) {
                const upateOrder = 'UPDATE Orders SET status_order = ? WHERE id = ?';
                const upateOrder_query = mysql.format(upateOrder, [statusOrder, orderNumber]);
    
                await connection.query(upateOrder_query, (err, result2) => {
                    if(result2 !== 0){
                        res.status(201).json({message: 'Order status updated !'});
                    }
                })
            }
        })
    })
}

function getOrders(req,res) {
    const sqlSearchUserID = 'SELECT id FROM User WHERE email = ?'
    const searchUserID_query = mysql.format(sqlSearchUserID,[req.user.user])

    db.getConnection(async (err,connection)=> {
        await connection.query(searchUserID_query, async (err, result) => {
            if(result.length !== 0) {
                const searchOrder = 'SELECT id, status_order,product_name,package_name,order_price,quantity_order FROM Orders WHERE id_user = ?';
                const searchOrder_query = mysql.format(searchOrder, [result[0].id]);
    
                await connection.query(searchOrder_query, (err, result2) => {
                    if(result2 !== 0){
                        let message = [];
                        for(let i = 0; i < result2.length; i++){
                            message.push('Order number '+ result2[i].id+ " , Status : "+ result2[i].status_order+" , Product : "+ result2[i].product_name+" , Package : "+result2[i].package_name+" , Quantity : "+result2[i].quantity_order+ " Order price : "+result2[0].order_price+" €")
                        }
                        res.status(201).json({...message, data: {...result2}});
                    }
                })
            }
        })
    })
}

module.exports = {
    getOrders,
    updateOrder
}
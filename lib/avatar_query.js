const mysql = require('mysql');
const db = require('../db').con;

function createAvatar(req,res) {
    const options = req.body.options;
    db.getConnection( async(err, connection) => {
        if(err) throw(err);

        const sqlSearch = 'SELECT * FROM Avatar WHERE name = ?'
        const search_query = mysql.format(sqlSearch,[req.user.user]);
        const sqlSearchUserID = 'SELECT id FROM User WHERE email = ?'
        const searchUserID_query = mysql.format(sqlSearchUserID,[req.user.user])

        const sqlInsert = 'INSERT INTO Avatar VALUES(0,?,?,?)'

        await connection.query(search_query, async(err, resultat) => {
            if(err) throw(err);

            if(resultat.length !== 0){
                connection.release();
                console.log('Avatar already exits');
                res.sendStatus(409);
            }
            else {
                await connection.query(searchUserID_query, async(err, resultat) => {
                    if(err) throw(err);

                    if(resultat.length !== 0){
                        const insert_query = [];
                        for(let i = 0;i <options.length;i++){
                            insert_query.push(mysql.format(sqlInsert,[resultat[0].id,req.user.user,options[i]]));
                        }
                        for (let i = 0; i < insert_query.length; i++) {
                            await connection.query(insert_query[i], async(err, result) => {
                                if(err) throw(err);
                                if(result.length !== 0){
                                    console.log('Avatar createad', result.insertId);
                                    res.sendStatus(201);
                                }
                            })
                        }                    
                    }
                })
            }
        })
    })
}

module.exports = {
    createAvatar
}
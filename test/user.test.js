const request = require('supertest');
const app = require('../server');
describe('User test', () => {
    describe('User create', ()=> {
        beforeEach(async()=> {
            const res = await request(app).delete("/user/delete").send({
                "email": "test@test.fr"
            });
        })
        test('POST /create', async()=> {
            const res = await request(app).post("/user/create").send({
                "username": "test", 
                "password": "test",
                "email": "test@test.fr"
            });
            expect(res.statusCode).toEqual(201);
        });
    })

    beforeEach(async()=> {
        const res = await request(app).post("/user/create").send({
            "username": "test", 
            "password": "test",
            "email": "test@test.fr"
        });
    })
    test('POST /login', async()=> {
        const res = await request(app).post("/user/login").send({
            "password": "test",
            "email": "test@test.fr"
        });
        expect(res.statusCode).toEqual(200);
    });
    test('POST /logout', async()=> {
        const res = await request(app).post("/user/logout")
        expect(res.statusCode).toEqual(204);
    });
})